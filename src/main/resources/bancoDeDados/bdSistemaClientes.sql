-- MySQL Script generated by MySQL Workbench
-- Thu Apr  5 10:40:29 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema projetoprogramacaoiibd
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema projetoprogramacaoiibd
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `projetoprogramacaoiibd` DEFAULT CHARACTER SET utf8 ;
USE `projetoprogramacaoiibd` ;

-- -----------------------------------------------------
-- Table `projetoprogramacaoiibd`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetoprogramacaoiibd`.`cliente` (
  `idcliente` BIGINT(14) NOT NULL,
  `cpfcliente` VARCHAR(14) NOT NULL,
  `nome` VARCHAR(45) NOT NULL,
  `endereco` VARCHAR(45) NOT NULL,
  `sexo` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idcliente`),
  UNIQUE INDEX `cpfcliente_UNIQUE` (`cpfcliente` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetoprogramacaoiibd`.`produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetoprogramacaoiibd`.`produto` (
  `idproduto` BIGINT(14) NOT NULL,
  `descricao` VARCHAR(60) NOT NULL,
  `preco` DOUBLE NOT NULL,
  PRIMARY KEY (`idproduto`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetoprogramacaoiibd`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetoprogramacaoiibd`.`compra` (
  `idcompra` BIGINT(14) NOT NULL,
  `cliente_idcliente` BIGINT(14) NOT NULL,
  PRIMARY KEY (`idcompra`),
  INDEX `fk_compra_cliente1_idx` (`cliente_idcliente` ASC),
  CONSTRAINT `fk_compra_cliente1`
    FOREIGN KEY (`cliente_idcliente`)
    REFERENCES `projetoprogramacaoiibd`.`cliente` (`idcliente`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `projetoprogramacaoiibd`.`item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `projetoprogramacaoiibd`.`item` (
  `iditem` BIGINT(14) NOT NULL,
  `qtd` INT NOT NULL,
  `compra_idcompra` BIGINT(14) NOT NULL,
  `produto_idproduto` BIGINT(14) NOT NULL,
  PRIMARY KEY (`iditem`),
  INDEX `fk_item_compra1_idx` (`compra_idcompra` ASC),
  INDEX `fk_item_produto1_idx` (`produto_idproduto` ASC),
  CONSTRAINT `fk_item_compra1`
    FOREIGN KEY (`compra_idcompra`)
    REFERENCES `projetoprogramacaoiibd`.`compra` (`idcompra`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_item_produto1`
    FOREIGN KEY (`produto_idproduto`)
    REFERENCES `projetoprogramacaoiibd`.`produto` (`idproduto`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
