-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19-Abr-2018 às 22:01
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetoprogramacaoiibd`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` bigint(14) NOT NULL,
  `cpfcliente` varchar(14) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `endereco` varchar(45) NOT NULL,
  `sexo` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`idcliente`, `cpfcliente`, `nome`, `endereco`, `sexo`) VALUES
(3, '000.000.000-28', 'Bira2', 'qd 112 lt 29', 'masculino'),
(4, '000.000.000-25', 'Bira', 'qd 112 lt 29', 'masculino'),
(5, '000.000.000-23', 'Bira', 'qd 112 lt 29', 'masculino'),
(6, '000.000.000-22', 'Joao', 'qd 11 lt 26', 'masculino'),
(7, '000.258.000-15', 'Jose Nildo', 'qd 1', 'masculino'),
(8, '034.526.856-82', 'Ubiratan Leitão Mourão', 'Qd 112 Lt 29 Parque XIV', 'masculino');

-- --------------------------------------------------------

--
-- Estrutura da tabela `compra`
--

CREATE TABLE `compra` (
  `idcompra` bigint(14) NOT NULL,
  `cliente_idcliente` bigint(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `compra`
--

INSERT INTO `compra` (`idcompra`, `cliente_idcliente`) VALUES
(9, 4),
(10, 6),
(11, 8),
(12, 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `item`
--

CREATE TABLE `item` (
  `iditem` bigint(14) NOT NULL,
  `qtd` int(11) NOT NULL,
  `compra_idcompra` bigint(14) NOT NULL,
  `produto_idproduto` bigint(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `item`
--

INSERT INTO `item` (`iditem`, `qtd`, `compra_idcompra`, `produto_idproduto`) VALUES
(4, 2, 9, 0),
(5, 1, 11, 4),
(6, 1, 11, 5),
(7, 1, 12, 5),
(8, 1, 12, 4),
(9, 2, 12, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `idproduto` bigint(14) NOT NULL,
  `descricao` varchar(60) NOT NULL,
  `preco` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`idproduto`, `descricao`, `preco`) VALUES
(0, 'certificado Digital', 150),
(1, 'Boneco Minions', 30),
(2, 'certificado', 150),
(3, 'produto', 1500),
(4, 'Xbox One', 1500),
(5, 'Smart TV 50\" 4K', 7000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD UNIQUE KEY `cpfcliente_UNIQUE` (`cpfcliente`);

--
-- Indexes for table `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idcompra`),
  ADD KEY `fk_compra_cliente1_idx` (`cliente_idcliente`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`iditem`),
  ADD KEY `fk_item_compra1_idx` (`compra_idcompra`),
  ADD KEY `fk_item_produto1_idx` (`produto_idproduto`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`idproduto`);

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_compra_cliente1` FOREIGN KEY (`cliente_idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `fk_item_compra1` FOREIGN KEY (`compra_idcompra`) REFERENCES `compra` (`idcompra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_item_produto1` FOREIGN KEY (`produto_idproduto`) REFERENCES `produto` (`idproduto`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
