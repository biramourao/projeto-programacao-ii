package br.edu.programacao.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;

public class CompraTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = -5257255345494752481L;
	private List<Compra> compras;
    private String[] colunas = {"Cod. da Copra", "CPF do Cliente", "Nome do Cliente", "Qtd de Itens" , "Valor Total"};
     
    public CompraTableModel() {
        compras = new ArrayList<Compra>();
    }
    public CompraTableModel(List<Compra> pCompras) {
    	compras = pCompras;
        this.fireTableDataChanged();
	}
     
    public void addRow(Compra c){
        this.compras.add(c);
        this.fireTableDataChanged();
    }
 
    public String getColumnName(int num){
        return this.colunas[num];
    }
 
    
    public int getRowCount() {
        return compras.size();
    }
    
    
    public int getColumnCount() {
        return colunas.length;
    }
 
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0: return compras.get(linha).getIdCompra();
            case 1: return compras.get(linha).getCliente().getCpf();
            case 2: return compras.get(linha).getCliente().getNome();
            case 3: return compras.get(linha).getItens().size();
            case 4:
            	double total = 0;
            	for(Item item : compras.get(linha).getItens()) {
            		total += item.getQuantidade() * item.getProduto().getPreco();
            	}
            	return total;
        }   
        return null;
    }
    
    public void removeRow(int linha){
        this.compras.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }
    
    public Compra getItem(int linha){
        return this.compras.get(linha);
    }
    public void setItem(int linha, Compra c) {
    	this.compras.set(linha, c);
    	this.fireTableRowsUpdated(linha, linha);
    }
    
    public boolean isCellEditable(int linha, int coluna) {
        return false;
    }  

}
