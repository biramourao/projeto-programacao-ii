package br.edu.programacao.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.edu.programacao.model.Cliente;

public class ClienteTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1593642744278005894L;
	private List<Cliente> clientes;
    private String[] colunas = {"ID" , "CPF" ,"Nome","Sexo", "Endereo"};
     
    public ClienteTableModel() {
        clientes = new ArrayList<Cliente>();
    }
    public ClienteTableModel(List<Cliente> pClientes) {
    	clientes = pClientes;
        this.fireTableDataChanged();
	}
     
    public void addRow(Cliente c){
        this.clientes.add(c);
        this.fireTableDataChanged();
    }
 
    public String getColumnName(int num){
        return this.colunas[num];
    }
 
    
    public int getRowCount() {
        return clientes.size();
    }
    
    
    public int getColumnCount() {
        return colunas.length;
    }
 
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0: return clientes.get(linha).getIdCliente();
            case 1: return clientes.get(linha).getCpf();
            case 2: return clientes.get(linha).getNome();
            case 3: return clientes.get(linha).getSexo();
            case 4: return clientes.get(linha).getEndereco();
        }   
        return null;
    }
    
    public void removeRow(int linha){
        this.clientes.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }
    
    public Cliente getCliente(int linha){
        return this.clientes.get(linha);
    }
    public void setCliente(int linha, Cliente c) {
    	this.clientes.set(linha, c);
    	this.fireTableRowsUpdated(linha, linha);
    }
    
    public boolean isCellEditable(int linha, int coluna) {
        return false;
    }  

}
