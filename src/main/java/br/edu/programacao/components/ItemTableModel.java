package br.edu.programacao.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.edu.programacao.model.Item;

public class ItemTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = -5257255345494752481L;
	private List<Item> itens;
    private String[] colunas = {"Descrição" , "Quantidade", "Preço Unit", "Preço Total"};
     
    public ItemTableModel() {
        itens = new ArrayList<Item>();
    }
    public ItemTableModel(List<Item> pItens) {
    	itens = pItens;
        this.fireTableDataChanged();
	}
     
    public void addRow(Item i){
        this.itens.add(i);
        this.fireTableDataChanged();
    }
 
    public String getColumnName(int num){
        return this.colunas[num];
    }
 
    
    public int getRowCount() {
        return itens.size();
    }
    
    
    public int getColumnCount() {
        return colunas.length;
    }
 
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0: return itens.get(linha).getProduto().getDescricao();
            case 1: return itens.get(linha).getQuantidade();
            case 2: return ""+itens.get(linha).getProduto().getPreco();
            case 3: return ""+((itens.get(linha).getProduto().getPreco())*(itens.get(linha).getQuantidade()));
        }   
        return null;
    }
    
    public void removeRow(int linha){
        this.itens.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }
    
    public Item getItem(int linha){
        return this.itens.get(linha);
    }
    public void setItem(int linha, Item i) {
    	this.itens.set(linha, i);
    	this.fireTableRowsUpdated(linha, linha);
    }
    
    public boolean isCellEditable(int linha, int coluna) {
        return false;
    }  

}
