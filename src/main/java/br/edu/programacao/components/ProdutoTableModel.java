package br.edu.programacao.components;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import br.edu.programacao.model.Produto;

public class ProdutoTableModel extends AbstractTableModel {

	
	private static final long serialVersionUID = -5257255345494752481L;
	private List<Produto> produtos;
    private String[] colunas = {"ID" , "Descrição" ,"Preço"};
     
    public ProdutoTableModel() {
        produtos = new ArrayList<Produto>();
    }
    public ProdutoTableModel(List<Produto> pProdutos) {
    	produtos = pProdutos;
        this.fireTableDataChanged();
	}
     
    public void addRow(Produto p){
        this.produtos.add(p);
        this.fireTableDataChanged();
    }
 
    public String getColumnName(int num){
        return this.colunas[num];
    }
 
    
    public int getRowCount() {
        return produtos.size();
    }
    
    
    public int getColumnCount() {
        return colunas.length;
    }
 
    public Object getValueAt(int linha, int coluna) {
        switch(coluna){
            case 0: return produtos.get(linha).getIdProduto();
            case 1: return produtos.get(linha).getDescricao();
            case 2: return produtos.get(linha).getPreco();
        }   
        return null;
    }
    
    public void removeRow(int linha){
        this.produtos.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }
    
    public Produto getProduto(int linha){
        return this.produtos.get(linha);
    }
    public void setProduto(int linha, Produto p) {
    	this.produtos.set(linha, p);
    	this.fireTableRowsUpdated(linha, linha);
    }
    
    public boolean isCellEditable(int linha, int coluna) {
        return false;
    }  

}
