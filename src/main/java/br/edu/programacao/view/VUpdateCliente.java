package br.edu.programacao.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.text.MaskFormatter;

import br.edu.programacao.DAO.ClienteDAO;
import br.edu.programacao.controller.CCliente;
import br.edu.programacao.model.Cliente;

public class VUpdateCliente extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Container container;
	Label lbCpf;
	JFormattedTextField tfCpf;
	Label lbNome;
	TextField tfNome;
	Label lbSexo;
	JRadioButton masculino,
    feminino;
	Label lbEndereco;
	TextField tfEndereco;
	JButton btConfirmar;
	JButton btCancelar;
	String sexo;
	CCliente controlador = CCliente.getInstance();
	Cliente cliente;
	
	public VUpdateCliente(Cliente pCliente) {
		this.cliente = pCliente;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Editar Cliente");
		setLocationRelativeTo(null);
		setSize(500,200);
		setResizable(false);
		/*
		 * private String cpf;
		 * private String nome;
		 * private String sexo;
		 * private String endereco;
		 * */
		
		container = getContentPane();
		container.setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		Container linha1 = new Container();
		Container linha2 = new Container();
		Container linha3 = new Container();
		Container linha4 = new Container();

		
		FlowLayout layoutFlow = new FlowLayout();
		layoutFlow.setAlignment(FlowLayout.CENTER);
		linha1.setLayout(layoutFlow);
		container.add(linha1);
		
		MaskFormatter mascaraCpf = new MaskFormatter();
		try {
			mascaraCpf = new MaskFormatter("###.###.###-##");
			mascaraCpf.setPlaceholderCharacter('_');
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		lbCpf = new Label("CPF:");
		linha1.add(lbCpf);
		tfCpf = new JFormattedTextField(mascaraCpf);
		tfCpf.setColumns(14);
		
		linha1.add(tfCpf);
		
		lbNome = new Label("Nome:");
		linha1.add(lbNome);
		tfNome = new TextField();
		tfNome.setColumns(25);
		linha1.add(tfNome);
		
		linha2.setLayout(layoutFlow);
		container.add(linha2);
		
		lbSexo = new Label("Sexo: ");
		masculino = new JRadioButton("Masculino", false);
		feminino = new JRadioButton("Feminino", false);
		linha2.add(lbSexo);
		linha2.add(masculino);
		linha2.add(feminino);
		
		masculino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				feminino.setSelected(false);
				sexo = "masculino";
			}
		});;
		feminino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				masculino.setSelected(false);
				sexo = "feminino";
			}
		});
		
		linha3.setLayout(layoutFlow);
		container.add(linha3);
		
		lbEndereco = new Label("Endereço: ");
		linha3.add(lbEndereco);
		tfEndereco = new TextField();
		tfEndereco.setColumns(45);
		linha3.add(tfEndereco);
		
		linha4.setLayout(layoutFlow);
		container.add(linha4);
		
		btConfirmar = new JButton("Confirmar");
		linha4.add(btConfirmar);
		btConfirmar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				cliente.setCpf(tfCpf.getText());
				cliente.setNome(tfNome.getText());
				cliente.setSexo(sexo);
				cliente.setEndereco(tfEndereco.getText());
				
				try {
					int resultado = controlador.editarCliente(cliente);
					if (resultado == 1) {
						JOptionPane.showMessageDialog(null, "Cliente alterado com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (SQLException erro) {
					JOptionPane.showMessageDialog(null, "Erro ao alterar cliente. \nDetalhes: " + erro.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
				}
				dispose();
			}
		});
		
		btCancelar = new JButton("Cancelar");
		linha4.add(btCancelar);
		setVisible(true);
		btCancelar.addActionListener(new ActionListener() {
					
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		tfCpf.setText(pCliente.getCpf());
		tfNome.setText(pCliente.getNome());
		tfEndereco.setText(pCliente.getEndereco());
		if(pCliente.getSexo().equalsIgnoreCase("masculino")){
			masculino.setSelected(true);
			feminino.setSelected(false);
			sexo = "masculino";
		}else if (pCliente.getSexo().equalsIgnoreCase("feminino")) {
			feminino.setSelected(true);
			masculino.setSelected(false);
			sexo = "feminino";
		}
	}

	public static void main(String[] args) {
		Cliente cliente = null;
		try {
			cliente = new ClienteDAO().readOne(1);
		} catch (SQLException error) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Erro ao alterar cliente. \nDetalhes: " + error.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		new VUpdateCliente(cliente);
	}

}
