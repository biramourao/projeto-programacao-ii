package br.edu.programacao.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import br.edu.programacao.components.ClienteTableModel;
import br.edu.programacao.controller.CCliente;
import br.edu.programacao.model.Cliente;

public class VSelecionarCliente extends JFrame {

	private static final long serialVersionUID = -4757051083486256837L;
	private JPanel dadosPanel;
	private JPanel botoesPanel;
	private JButton btnSelecionar;
	private JTable dadosTabela;
	private JScrollPane scrollTabela;
	private CCliente controlador = CCliente.getInstance();
	private List<Cliente> dados = new ArrayList<Cliente>();
	private Cliente clienteCorrente = null;

	public VSelecionarCliente() {
		super("Selecionar Clientes");
		try {
			dados = controlador.getClienteListBD();
		} catch (SQLException e2) {
			JOptionPane.showMessageDialog(null, "Erro ao preencher lista. \nDetalhes: " + e2.getMessage(), "Erro!",
					JOptionPane.ERROR_MESSAGE);
			e2.printStackTrace();
		}
		dadosTabela = new JTable(dadosDaTabela());
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		// modeloTabela = new ClienteTableModel(dados);
		// modeloTabela.setColumnIdentifiers(cabecalho);

		// Iterator<Cliente> iterator = clientes.iterator();
		// while (iterator.hasNext()){
		// Cliente it = iterator.next();
		// codigos.add(it.getIdCliente());
		// modeloTabela.addRow(new Object[] {it.getIdCliente(), it.getCpf(),
		// it.getNome(), it.getSexo(), it.getEndereco()});
		// }
		// dadosTabela = new JTable(modeloTabela);
		scrollTabela = new JScrollPane(dadosTabela);
		scrollTabela.setPreferredSize(new Dimension(600, 325));
		/**
		 * Desenha a tabela em toda a área disponível
		 */
		dadosTabela.setFillsViewportHeight(true);

		dadosPanel = new JPanel();
		dadosPanel.setLayout(new GridLayout(1, 1));
		dadosPanel.add(scrollTabela);

		btnSelecionar = new JButton("Selecionar");
		botoesPanel = new JPanel();
		botoesPanel.setLayout(new FlowLayout());
		botoesPanel.add(btnSelecionar);

		/**
		 * Funciona quando adicionado ao getContentPane()
		 */
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(dadosPanel, BorderLayout.PAGE_START);
		getContentPane().add(botoesPanel);

		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				dadosTabela.setModel(dadosDaTabela());
				dadosPanel.updateUI();
				super.windowGainedFocus(e);
			}
		});
		
		btnSelecionar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int x = dadosTabela.getSelectedRow();
				clienteCorrente = dados.get(x);
				JOptionPane.showMessageDialog(null, "Cliente " + clienteCorrente.getNome() + " selecionado!", "Cliente Selecionado", JOptionPane.INFORMATION_MESSAGE);
				if(clienteCorrente!=null) {
					new VComprar(clienteCorrente);
					dispose();
				}
			}
		});

		setSize(600, 400);
		/**
		 * Sempre colocar setSize antes de setLocationRelativeTo
		 */
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	private TableModel dadosDaTabela() {
		List<Cliente> cList = null;
		try {
			cList = controlador.getClienteListBD();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ClienteTableModel model = new ClienteTableModel();

		for (Cliente c : cList) {
			model.addRow(c);
		}

		return model;
	}
	public Cliente getClienteCorrente() {
		return clienteCorrente;
	}
	public static void main(String[] args) {
		new VSelecionarCliente();
	}

}
