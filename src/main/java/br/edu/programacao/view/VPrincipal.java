package br.edu.programacao.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class VPrincipal extends JFrame {
	
	private static final long serialVersionUID = -2624123197161813355L;
	private Container contentPane;
	private Container botoes;
	private Container logo;
	private JButton cadastrarCliente = new JButton("<html>Cadastrar<br>Cliente</html>");
	private JButton editarExcluirCliente = new JButton("<html>Editar/Excluir<br>Clientes</html>");
	private JButton cadastrarProduto = new JButton("<html>Cadastrar<br>Produto</html>");
	private JButton editarExcluirProduto = new JButton("<html>Editar/Excluir<br>Produtos</html>");
	private JButton comprar = new JButton("<html>Comprar</html>");
	private JButton editarExcluirCompras = new JButton("<html>Editar/Excluir<br>Compras</html>");
	private JLabel logoSistema = new JLabel();

	public VPrincipal() {
		super("Sistema de Compras");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(1000, 600);
        setLocationRelativeTo(null);
        
        contentPane = getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane,BoxLayout.PAGE_AXIS));
		
        Image comprarIcon;
		try {
			comprarIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/buy.png"));
			comprar.setIcon(new ImageIcon(comprarIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image addBoxIcon;
		try {
			addBoxIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/addBox.png"));
			cadastrarProduto.setIcon(new ImageIcon(addBoxIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image addUserIcon;
		try {
			addUserIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/addUser.png"));
			cadastrarCliente.setIcon(new ImageIcon(addUserIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image editBoxIcon;
		try {
			editBoxIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/editBox.png"));
			editarExcluirProduto.setIcon(new ImageIcon(editBoxIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image editUserIcon;
		try {
			editUserIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/editUser.png"));
			editarExcluirCliente.setIcon(new ImageIcon(editUserIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Image editBuyIcon;
		try {
			editBuyIcon = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/editBuy.png"));
			editarExcluirCompras.setIcon(new ImageIcon(editBuyIcon));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
        botoes = new Container();
        botoes.setLayout(new FlowLayout(FlowLayout.LEFT));
        botoes.setSize(800, 60);
        botoes.add(comprar);
        botoes.add(editarExcluirCompras);
        botoes.add(cadastrarCliente);
        botoes.add(editarExcluirCliente);
        botoes.add(cadastrarProduto);
        botoes.add(editarExcluirProduto);        
        
        logo = new Container();
		logo.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		Image logoImg;
		try {
			logoImg = ImageIO.read(getClass().getResource("/br/edu/programacao/icons/online-shopping.png"));
			logoSistema.setIcon((new ImageIcon(logoImg)));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logo.add(logoSistema);
		contentPane.add(botoes);
		contentPane.add(new JSeparator());
		contentPane.add(logo);
		
        JMenuBar menuBar = new JMenuBar();        
        setJMenuBar(menuBar);        
        JMenu comprarMenu = new JMenu("Comprar");
        JMenu cadastrarMenu = new JMenu("Cadastrar");
        JMenu editarExcluirMenu = new JMenu("Editar/Excluir");
        JMenu sairMenu = new JMenu("Sair");
        
        menuBar.add(comprarMenu);
        menuBar.add(cadastrarMenu);
        menuBar.add(editarExcluirMenu);
        menuBar.add(sairMenu);

        JMenuItem comprarAction = new JMenuItem("Comprar");
        JMenuItem cadastrarClienteAction = new JMenuItem("Cadastrar cliente");
        JMenuItem cadastrarProdutoAction = new JMenuItem("Cadastrar produto");
        JMenuItem editarExcluirClienteAction = new JMenuItem("Editar/Excluir cliente");
        JMenuItem editarExcluirProdutoAction = new JMenuItem("Editar/Excluir produto");
        JMenuItem editarExcluirCompraAction = new JMenuItem("Editar/Excluir compra");
        JMenuItem sairAction = new JMenuItem("Sair");
        
        comprarMenu.add(comprarAction);
        
        cadastrarMenu.add(cadastrarClienteAction);
        cadastrarMenu.add(cadastrarProdutoAction);
        
        editarExcluirMenu.add(editarExcluirClienteAction);
        editarExcluirMenu.add(editarExcluirCompraAction);
        editarExcluirMenu.add(editarExcluirProdutoAction);
        
        sairMenu.add(sairAction);
        
        ActionListener cadastrarClienteAL = new ActionListener() {		
			public void actionPerformed(ActionEvent e) {
				new VInsertCliente();
			}
		};
		cadastrarCliente.addActionListener(cadastrarClienteAL);
        cadastrarClienteAction.addActionListener(cadastrarClienteAL);
        
		ActionListener editarClienteAL = new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				new VListUpdateDeleteClientes();
			}
		};
        editarExcluirCliente.addActionListener(editarClienteAL);
        editarExcluirClienteAction.addActionListener(editarClienteAL);
        
        ActionListener cadastrarProdutoAL = new ActionListener() {		
			public void actionPerformed(ActionEvent e) {
				new VInsertProduto();
			}
		};
        cadastrarProduto.addActionListener(cadastrarProdutoAL);
        cadastrarProdutoAction.addActionListener(cadastrarProdutoAL);
        
        ActionListener editarProdutoAL = new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				new VListUpdateDeleteProdutos();
			}
		};        
        editarExcluirProdutoAction.addActionListener(editarProdutoAL);
        editarExcluirProduto.addActionListener(editarProdutoAL);
        
        ActionListener comprarAL = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				new VSelecionarCliente();			
			}
		};
		comprar.addActionListener(comprarAL);
		comprarAction.addActionListener(comprarAL);
		ActionListener editarCompraAL = new ActionListener() {	
			public void actionPerformed(ActionEvent e) {
				new VListUpdateDeleteCompras();
			}
		};
		editarExcluirCompraAction.addActionListener(editarCompraAL);
		editarExcluirCompras.addActionListener(editarCompraAL);
		
		sairAction.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
        setVisible(true);
	}

	public static void main(String[] args) {
        new VPrincipal();
	}

}
