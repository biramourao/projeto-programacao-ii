package br.edu.programacao.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import br.edu.programacao.components.ProdutoTableModel;
import br.edu.programacao.controller.CItem;
import br.edu.programacao.controller.CProduto;
import br.edu.programacao.model.Item;
import br.edu.programacao.model.Produto;

public class VAdicionarItem extends JFrame {

	private static final long serialVersionUID = -3206285249465041100L;
	private JPanel dadosPanel;
	private JPanel botoesPanel;
	private JButton btnConfirmar;
	private JTable dadosTabela;
	private JScrollPane scrollTabela;
	private CProduto controladorProduto = CProduto.getInstance();
	private CItem controladorItem = CItem.getInstance();
	private List<Produto> dados = new ArrayList<Produto>();
	private int idCompra = 0;

	public VAdicionarItem(int pIdCompra) {
		super("Adicionar Item");
		idCompra = pIdCompra;
		try {
			dados = controladorProduto.getProdutoListBD();
		} catch (SQLException e2) {
			JOptionPane.showMessageDialog(null, "Erro ao preencher lista. \nDetalhes: " + e2.getMessage(), "Erro!",
					JOptionPane.ERROR_MESSAGE);
			e2.printStackTrace();
		}
		dadosTabela = new JTable(dadosDaTabela());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		scrollTabela = new JScrollPane(dadosTabela);
		scrollTabela.setPreferredSize(new Dimension(600, 325));
		/**
		 * Desenha a tabela em toda a área disponível
		 */
		dadosTabela.setFillsViewportHeight(true);

		dadosPanel = new JPanel();
		dadosPanel.setLayout(new GridLayout(1, 1));
		dadosPanel.add(scrollTabela);

		btnConfirmar = new JButton("Adicionar");
		botoesPanel = new JPanel();
		botoesPanel.setLayout(new FlowLayout());
		botoesPanel.add(btnConfirmar);

		/**
		 * Funciona quando adicionado ao getContentPane()
		 */
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(dadosPanel, BorderLayout.PAGE_START);
		getContentPane().add(botoesPanel);

		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				dadosTabela.setModel(dadosDaTabela());
				dadosPanel.updateUI();
				super.windowGainedFocus(e);
			}
		});


		btnConfirmar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int x = dadosTabela.getSelectedRow();
				int quantidade = 0;
				quantidade = Integer.parseInt((JOptionPane.showInputDialog(null, "Qual a quantidade de " + dados.get(x).getDescricao(), "Informe a Quantidade", JOptionPane.QUESTION_MESSAGE)));
				Item item = new Item();
				item.setIdCompra(idCompra);
				item.setProduto(dados.get(x));
				item.setQuantidade(quantidade);
				try {
					controladorItem.adicionarItem(item);
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, "Erro ao inserir produto. Detalhes: " + e1, "Erro!", JOptionPane.INFORMATION_MESSAGE);
				}
				dispose();
			}
		});

		setSize(600, 400);
		/**
		 * Sempre colocar setSize antes de setLocationRelativeTo
		 */
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	private TableModel dadosDaTabela() {
		List<Produto> pList = null;
		try {
			pList = controladorProduto.getProdutoListBD();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ProdutoTableModel model = new ProdutoTableModel();

		for (Produto p : pList) {
			model.addRow(p);
		}

		return model;
	}

}
