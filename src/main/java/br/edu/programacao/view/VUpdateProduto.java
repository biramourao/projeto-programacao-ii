package br.edu.programacao.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DecimalFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.edu.programacao.DAO.ProdutoDAO;
import br.edu.programacao.components.JMoneyField;
import br.edu.programacao.controller.CProduto;
import br.edu.programacao.model.Produto;

public class VUpdateProduto extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Container container;
	Label lbDescricao;
	JTextField tfDescricao;
	Label lbPreco;
	JMoneyField tfPreco;
	JButton btnConfirmar;
	JButton btCancelar;
	Produto produto;

	final CProduto controlador = CProduto.getInstance();

	public VUpdateProduto(Produto pProduto) {
		produto = pProduto;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Editar Produto");
		setLocationRelativeTo(null);
		setSize(500, 150);
		setResizable(false);

		container = getContentPane();
		container.setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		Container linha1 = new Container();
		Container linha2 = new Container();

		FlowLayout layoutFlow = new FlowLayout();
		layoutFlow.setAlignment(FlowLayout.CENTER);
		linha1.setLayout(layoutFlow);
		container.add(linha1);

		lbDescricao = new Label("Descrição:");
		linha1.add(lbDescricao);
		tfDescricao = new JTextField();
		tfDescricao.setColumns(14);

		linha1.add(tfDescricao);

		lbPreco = new Label("Preço:");
		linha1.add(lbPreco);
		tfPreco = new JMoneyField();
		tfPreco.setColumns(10);
		tfPreco.setEditable(true);
		linha1.add(tfPreco);

		linha2.setLayout(layoutFlow);
		container.add(linha2);

		btnConfirmar = new JButton("Confirmar");
		linha2.add(btnConfirmar);

		btCancelar = new JButton("Cancelar");
		linha2.add(btCancelar);
		setVisible(true);

		tfDescricao.setText(produto.getDescricao());
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		tfPreco.setText(decimalFormat.format(produto.getPreco()));

		btCancelar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnConfirmar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				produto.setDescricao(tfDescricao.getText());
				produto.setPreco(Double.parseDouble(tfPreco.getText()));

				try {
					int resultado = controlador.editarProduto(produto);
					if (resultado == 1) {
						JOptionPane.showMessageDialog(null, "Produto Editado com Sucesso", "Sucesso",
								JOptionPane.INFORMATION_MESSAGE);
						dispose();
					}
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,
							"Erro ao editar produto!\nVerifique os dados inseridos e tente novamente. \nDetalhes: "
									+ e1.getMessage(),
							"Erro!", JOptionPane.ERROR_MESSAGE);
					e1.printStackTrace();
				}

			}
		});
	}

	public static void main(String[] args) {
		Produto produto = new Produto();
		try {
			produto = new ProdutoDAO().readOne(2);
		} catch (SQLException error) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Erro ao ler produto. \nDetalhes: " + error.getMessage(), "Erro!",
					JOptionPane.ERROR_MESSAGE);
		}
		new VUpdateProduto(produto);

	}

}
