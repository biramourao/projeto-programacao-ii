package br.edu.programacao.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.edu.programacao.components.JMoneyField;
import br.edu.programacao.controller.CProduto;
import br.edu.programacao.model.Produto;

public class VInsertProduto extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Container container;
	Label lbDescricao;
	JTextField tfDescricao;
	Label lbPreco;
	JMoneyField tfPreco;
	JButton btCadastrar;
	JButton btCancelar;
	
	final CProduto controlador = CProduto.getInstance();

	public VInsertProduto() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Cadastrar Produto");
		setSize(500, 150);
		setLocationRelativeTo(null);
		setResizable(false);

		container = getContentPane();
		container.setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		Container linha1 = new Container();
		Container linha2 = new Container();

		FlowLayout layoutFlow = new FlowLayout();
		layoutFlow.setAlignment(FlowLayout.CENTER);
		linha1.setLayout(layoutFlow);
		container.add(linha1);

		lbDescricao = new Label("Descrição:");
		linha1.add(lbDescricao);
		tfDescricao = new JTextField();
		tfDescricao.setColumns(14);

		linha1.add(tfDescricao);

		lbPreco = new Label("Preço:");
		linha1.add(lbPreco);
		tfPreco = new JMoneyField();
		tfPreco.setColumns(10);
		tfPreco.setEditable(true);
		linha1.add(tfPreco);

		linha2.setLayout(layoutFlow);
		container.add(linha2);

		btCadastrar = new JButton("Cadastrar");
		linha2.add(btCadastrar);
		btCadastrar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				onClickCadastrar();
			}
		});

		btCancelar = new JButton("Cancelar");
		linha2.add(btCancelar);
		setVisible(true);
		btCancelar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

	}

	public void onClickCadastrar() {
		
		Produto produto = new Produto();
		produto.setDescricao(tfDescricao.getText());
		produto.setPreco(Double.parseDouble(tfPreco.getText()));
		
		try {
			int resultado = controlador.cadastrarProduto(produto);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Produto cadastrado com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
			dispose();
		} catch (SQLException erro) {
			JOptionPane.showMessageDialog(null, "Erro ao cadastrar produto. \nVerifique os dados e tente novamente!\nDetalhes: " + erro.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		VInsertProduto vInsertProduto = new VInsertProduto();

	}

}
