package br.edu.programacao.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import br.edu.programacao.components.ProdutoTableModel;
import br.edu.programacao.controller.CProduto;
import br.edu.programacao.model.Produto;

public class VListUpdateDeleteProdutos extends JFrame {

	private static final long serialVersionUID = -3206285249465041100L;
	private JPanel dadosPanel;
	private JPanel botoesPanel;
	private JButton btnVoltar;
	private JButton btnEditar;
	private JButton btnExcluir;
	private JTable dadosTabela;
	private JScrollPane scrollTabela;
	private CProduto controlador = CProduto.getInstance();
	private List<Produto> dados = new ArrayList<Produto>();

	public VListUpdateDeleteProdutos() {

		super("Produtos cadastrados");
		try {
			dados = controlador.getProdutoListBD();
		} catch (SQLException e2) {
			JOptionPane.showMessageDialog(null, "Erro ao preencher lista. \nDetalhes: " + e2.getMessage(), "Erro!",
					JOptionPane.ERROR_MESSAGE);
			e2.printStackTrace();
		}
		dadosTabela = new JTable(dadosDaTabela());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		// modeloTabela = new ClienteTableModel(dados);
		// modeloTabela.setColumnIdentifiers(cabecalho);

		// Iterator<Cliente> iterator = clientes.iterator();
		// while (iterator.hasNext()){
		// Cliente it = iterator.next();
		// codigos.add(it.getIdCliente());
		// modeloTabela.addRow(new Object[] {it.getIdCliente(), it.getCpf(),
		// it.getNome(), it.getSexo(), it.getEndereco()});
		// }
		// dadosTabela = new JTable(modeloTabela);
		scrollTabela = new JScrollPane(dadosTabela);
		scrollTabela.setPreferredSize(new Dimension(600, 325));
		/**
		 * Desenha a tabela em toda a área disponível
		 */
		dadosTabela.setFillsViewportHeight(true);

		dadosPanel = new JPanel();
		dadosPanel.setLayout(new GridLayout(1, 1));
		dadosPanel.add(scrollTabela);

		btnVoltar = new JButton("Voltar");
		btnEditar = new JButton("Editar");
		btnExcluir = new JButton("Excluir");
		botoesPanel = new JPanel();
		botoesPanel.setLayout(new FlowLayout());
		botoesPanel.add(btnEditar);
		botoesPanel.add(btnExcluir);
		botoesPanel.add(btnVoltar);

		/**
		 * Funciona quando adicionado ao getContentPane()
		 */
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(dadosPanel, BorderLayout.PAGE_START);
		getContentPane().add(botoesPanel);

		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				dadosTabela.setModel(dadosDaTabela());
				dadosPanel.updateUI();
				super.windowGainedFocus(e);
			}
		});

		btnVoltar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				dispose();
			}

		});
		btnEditar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int x = dadosTabela.getSelectedRow();
				try {
					controlador.openVUpdateProduto(dados.get(x).getIdProduto());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		btnExcluir.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				int x = dadosTabela.getSelectedRow();
				int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente excluir " + dados.get(x).getDescricao() + "?",
						"Deseja realmente excluir?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				if (resposta == JOptionPane.YES_OPTION) {
					try {
						controlador.excluirProduto(dados.get(x).getIdProduto());
						dados.remove(x);

					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "Erro ao excluir produto. \nDetalhes: " + e1.getMessage(),
								"Erro!", JOptionPane.ERROR_MESSAGE);
						e1.printStackTrace();
					}
				}

			}
		});

		setSize(600, 400);
		/**
		 * Sempre colocar setSize antes de setLocationRelativeTo
		 */
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	private TableModel dadosDaTabela() {
		List<Produto> pList = null;
		try {
			pList = controlador.getProdutoListBD();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ProdutoTableModel model = new ProdutoTableModel();

		for (Produto p : pList) {
			model.addRow(p);
		}

		return model;
	}

	public static void main(String[] args) {
		new VListUpdateDeleteProdutos();

	}

}
