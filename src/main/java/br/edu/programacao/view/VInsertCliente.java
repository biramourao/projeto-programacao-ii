package br.edu.programacao.view;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.text.MaskFormatter;

import br.edu.programacao.controller.CCliente;
import br.edu.programacao.model.Cliente;

public class VInsertCliente extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Container container;
	Label lbCpf;
	JFormattedTextField tfCpf;
	Label lbNome;
	TextField tfNome;
	Label lbSexo;
	JRadioButton masculino,
    feminino;
	Label lbEndereco;
	TextField tfEndereco;
	JButton btCadastrar;
	JButton btCancelar;
	String sexo;
	final CCliente controlador = CCliente.getInstance();
	
	public VInsertCliente() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Cadastrar Cliente");
		setSize(500,200);
		setLocationRelativeTo(null);
		setResizable(false);
		/*
		 * private String cpf;
		 * private String nome;
		 * private String sexo;
		 * private String endereco;
		 * */
		
		container = getContentPane();
		container.setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		Container linha1 = new Container();
		Container linha2 = new Container();
		Container linha3 = new Container();
		Container linha4 = new Container();

		
		FlowLayout layoutFlow = new FlowLayout();
		layoutFlow.setAlignment(FlowLayout.CENTER);
		linha1.setLayout(layoutFlow);
		container.add(linha1);
		
		MaskFormatter mascaraCpf = new MaskFormatter();
		try {
			mascaraCpf = new MaskFormatter("###.###.###-##");
			mascaraCpf.setPlaceholderCharacter('_');
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		lbCpf = new Label("CPF:");
		linha1.add(lbCpf);
		tfCpf = new JFormattedTextField(mascaraCpf);
		tfCpf.setColumns(14);
		
		linha1.add(tfCpf);
		
		lbNome = new Label("Nome:");
		linha1.add(lbNome);
		tfNome = new TextField();
		tfNome.setColumns(25);
		linha1.add(tfNome);
		
		linha2.setLayout(layoutFlow);
		container.add(linha2);
		
		lbSexo = new Label("Sexo: ");
		masculino = new JRadioButton("Masculino", false);
		feminino = new JRadioButton("Feminino", false);
		linha2.add(lbSexo);
		linha2.add(masculino);
		linha2.add(feminino);
		
		masculino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				feminino.setSelected(false);
				sexo = "masculino";
			}
		});;
		feminino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				masculino.setSelected(false);
				sexo = "feminino";
			}
		});
		
		linha3.setLayout(layoutFlow);
		container.add(linha3);
		
		lbEndereco = new Label("Endereço: ");
		linha3.add(lbEndereco);
		tfEndereco = new TextField();
		tfEndereco.setColumns(45);
		linha3.add(tfEndereco);
		
		linha4.setLayout(layoutFlow);
		container.add(linha4);
		
		btCadastrar = new JButton("Cadastrar");
		linha4.add(btCadastrar);
		btCadastrar.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				onClickCadastrar();
			}
		});
		
		btCancelar = new JButton("Cancelar");
		linha4.add(btCancelar);
		setVisible(true);
		btCancelar.addActionListener(new ActionListener() {
					
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	
	}
	public void onClickCadastrar() {
		Cliente cliente = new Cliente();
		cliente.setCpf(tfCpf.getText());
		cliente.setNome(tfNome.getText());
		cliente.setSexo(sexo);
		cliente.setEndereco(tfEndereco.getText());
		
		try {
			int resultado = controlador.cadastrarCliente(cliente);
			if (resultado == 1) {
				
				JOptionPane.showMessageDialog(null, "Cliente Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
				
				int resposta = JOptionPane.showConfirmDialog(null, "Deseja cadastrar outro Cliente?", "Cadastrar outro cliente?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(resposta==JOptionPane.YES_OPTION) {
					tfCpf.setText("");
					tfNome.setText("");
					tfEndereco.setText("");
					masculino.setSelected(false);
					feminino.setSelected(false);
				}else {
					dispose();
				}
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir cliente!\nVerifique os dados inseridos e tente novamente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		CCliente cCliente = CCliente.getInstance();
		VInsertCliente cadastrarCliente = new VInsertCliente();
	}

}
