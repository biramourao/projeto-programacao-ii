package br.edu.programacao.view;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import br.edu.programacao.components.ItemTableModel;
import br.edu.programacao.controller.CCompra;
import br.edu.programacao.controller.CItem;
import br.edu.programacao.model.Cliente;
import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;

public class VComprar extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5777432118727615713L;
	private final static CCompra controladorCompra = CCompra.getInstance();
	private final static CItem controladorItem = CItem.getInstance();
	private Cliente clienteCorrente = null;
	private Container contentPane;
	private JButton adicionarItem = new JButton("Adicionar Item");
	private JButton removerItem = new JButton("Remover Item");
	private JButton finalizarCompra = new JButton("Finalizar Compra");
	private JButton cancelarCompra = new JButton("Cancelar Compra");
	private JLabel lbValorTotalCopra = new JLabel("Total:");
	private JLabel lbClienteCorrente = new JLabel("Cliente: ");
	private JTextField tfValorTotalCompra = new JTextField();
	private JScrollPane scrollPane;
	private JTable tabelaItens;
	private Compra compra = new Compra();
	private JPanel itens;
	private JPanel botoes;
	private List<Item> list;

	public VComprar(Cliente pClienteCorrente) {
		super("Comprar");
		setSize(1000, 500);
		setLocationRelativeTo(null);
		contentPane = getContentPane();
		try {
			clienteCorrente = pClienteCorrente;
			compra.setCliente(clienteCorrente);
			compra.setIdCompra(controladorCompra.getProximoIdCompra());
			controladorCompra.insertCompra(compra);
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao iniciar compra! \nDetalhes: " + e, "Erro",
					JOptionPane.ERROR_MESSAGE);
		}

		lbClienteCorrente.setText("Cliente: " + clienteCorrente.getNome());
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.LINE_AXIS));

		tabelaItens = new JTable(dadosDaTabela());

		scrollPane = new JScrollPane(tabelaItens);
		scrollPane.setPreferredSize(new Dimension(800, 490));
		tabelaItens.setFillsViewportHeight(true);

		itens = new JPanel();
		itens.setLayout(new FlowLayout());
		itens.setSize(800, 500);
		itens.add(lbClienteCorrente);
		itens.add(scrollPane);

		botoes = new JPanel();
		botoes.setSize(30, 500);
		botoes.setLayout(new GridLayout(6, 1, 15, 15));
		botoes.add(adicionarItem);
		botoes.add(removerItem);
		botoes.add(cancelarCompra);

		botoes.add(finalizarCompra);
		lbValorTotalCopra.setFont(new Font(Font.SERIF, Font.BOLD, 22));
		botoes.add(lbValorTotalCopra);
		tfValorTotalCompra.setEditable(false);
		tfValorTotalCompra.setFont(new Font(Font.SERIF, Font.BOLD, 30));
		tfValorTotalCompra.setText("0.00");
		botoes.add(tfValorTotalCompra);

		contentPane.add(itens);
		contentPane.add(botoes);

		addWindowFocusListener(new WindowAdapter() {
			@Override
			public void windowGainedFocus(WindowEvent e) {
				float total = 0;
				tabelaItens.setModel(dadosDaTabela());
				itens.updateUI();
				for (Item item : list) {
					total += (item.getProduto().getPreco() * item.getQuantidade());
				}
				DecimalFormat dFormat = new DecimalFormat("R$ 0.00");
				tfValorTotalCompra.setText(String.valueOf(dFormat.format(total)));
				botoes.updateUI();
				super.windowLostFocus(e);
			}
		});
		adicionarItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				new VAdicionarItem(compra.getIdCompra());
			}
		});
		removerItem.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int x = tabelaItens.getSelectedRow();
				int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente remover o item selecionado?",
						"Deseja remover item?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (resposta == JOptionPane.YES_OPTION) {
					try {
						int resultado = controladorItem.excluirItem(list.get(x).getIdItem());
						if (resultado == 1) {
							list.remove(x);
						}

					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "Erro ao remover item! \nDetalhes: " + e1, "Erro",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});
		finalizarCompra.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (list.size() == 0) {
					int resposta = JOptionPane.showConfirmDialog(null,
							"Você não adicionou nenhum Item, deseja realmente finalizar? Sua compra será cancelada!",
							"Deseja finalizar?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (resposta == JOptionPane.YES_OPTION) {
						try {
							controladorCompra.excluirCompra(compra.getIdCompra());
							dispose();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}

					}
				} else {
					int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente finalizar a compra?",
							"Deseja finalizar?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
					if (resposta == JOptionPane.YES_OPTION) {
						dispose();
					}
				}

			}
		});
		cancelarCompra.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int resposta = JOptionPane.showConfirmDialog(null, "Deseja realmente cancelar a compra?",
						"Deseja cancelar?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (resposta == JOptionPane.YES_OPTION) {
					
						try {
							controladorCompra.excluirCompra(compra.getIdCompra());
							dispose();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
				}
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int resposta = JOptionPane.showConfirmDialog(null,
						"Deseja realmente fechar a janela? Essa ação acarretará no cancelamento da compra?",
						"Deseja fechar?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (resposta == JOptionPane.YES_OPTION) {
					try {
							controladorCompra.excluirCompra(compra.getIdCompra());
							dispose();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
				}
				super.windowClosing(e);
			}
		});

		setVisible(true);
	}

	private TableModel dadosDaTabela() {

		list = new ArrayList<Item>();
		if (compra.getIdCompra() != 0) {
			try {
				list = controladorItem.getItemListCompraBD(compra.getIdCompra());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		ItemTableModel model = new ItemTableModel();

		for (int i = 0; i < list.size(); i++) {
			Item item = list.get(i);
			model.addRow(item);
		}

		return model;
	}

}
