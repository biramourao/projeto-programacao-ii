package br.edu.programacao.controller;

import java.sql.SQLException;
import java.util.List;

import br.edu.programacao.DAO.ClienteDAO;
import br.edu.programacao.model.Cliente;
import br.edu.programacao.view.VUpdateCliente;

public class CCliente {

	private static CCliente cCliente = null;

	public static CCliente getInstance() {

		if (cCliente == null) {
			cCliente = new CCliente();
		}
		return cCliente;
	}

	private CCliente() {

	}
	
	public int cadastrarCliente(Cliente cliente) throws SQLException {
		
		return new ClienteDAO().insert(cliente);
		
	}
	public int editarCliente(Cliente cliente) throws SQLException {
		
		return new ClienteDAO().update(cliente);
	}
	
	public void openVUpdateCliente(int id) throws SQLException {
		Cliente cliente = new ClienteDAO().readOne(id);
		new VUpdateCliente(cliente);
	}
	
	public int excluirCliente(int id) throws SQLException {
		return new ClienteDAO().delete(id);
	}
	
	public List<Cliente> getClienteListBD() throws SQLException {
		return new ClienteDAO().listAll();
	}

}
