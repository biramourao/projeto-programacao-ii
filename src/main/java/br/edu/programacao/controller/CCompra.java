package br.edu.programacao.controller;

import java.sql.SQLException;
import java.util.List;

import br.edu.programacao.DAO.CompraDAO;
import br.edu.programacao.model.Compra;

public class CCompra {
	private static CCompra cCompra = null;

	public static CCompra getInstance() {

		if (cCompra == null) {
			cCompra = new CCompra();
		}
		return cCompra;
	}
	
	private CCompra() {
		
	}
	
	public int insertCompra(Compra compra) throws SQLException{
		return new CompraDAO().insert(compra);
	}
	
	public int editarCompra(Compra compra) throws SQLException{
		return new CompraDAO().update(compra);
	}
	
	public List<Compra> getCompraListBD() throws SQLException{
		return new CompraDAO().listAll();
	}
	
	public int excluirCompra(int idCompra) throws SQLException{
		return new CompraDAO().delete(idCompra);
	}
	
	public int getProximoIdCompra() throws SQLException {
		return new CompraDAO().proximoId();
	}
	
}
