package br.edu.programacao.controller;

import java.sql.SQLException;
import java.util.List;

import br.edu.programacao.DAO.ProdutoDAO;
import br.edu.programacao.model.Produto;
import br.edu.programacao.view.VUpdateProduto;

public class CProduto {
	private static CProduto cProduto = null;

	public static CProduto getInstance() {

		if (cProduto == null) {
			cProduto = new CProduto();
		}
		return cProduto;
	}

	private CProduto() {
		
	}
	
	public int cadastrarProduto(Produto produto) throws SQLException {
		
		return new ProdutoDAO().insert(produto);			
	}

	public int editarProduto(Produto produto) throws SQLException {
		
		return new ProdutoDAO().update(produto);
	}
	
	public void openVUpdateProduto(int id) throws SQLException {
		Produto produto = new ProdutoDAO().readOne(id);
		new VUpdateProduto(produto);
	}
	
	public List<Produto> getProdutoListBD() throws SQLException {
		return new ProdutoDAO().listAll();
	}

	public int excluirProduto(int idProduto) throws SQLException {
		return new ProdutoDAO().delete(idProduto);
		
	}
}
