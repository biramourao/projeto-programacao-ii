package br.edu.programacao.controller;

import java.sql.SQLException;
import java.util.List;

import br.edu.programacao.DAO.ItemDAO;
import br.edu.programacao.model.Item;

public class CItem {
		
	private static CItem cItem = null;

	public static CItem getInstance() {

		if (cItem == null) {
			cItem = new CItem();
		}
		return cItem;
	}	

	private CItem() {
		
	}
	
	public int adicionarItem(Item item) throws SQLException {
		
		return new ItemDAO().insert(item);			
	}

	public int editarItem(Item item) throws SQLException {
		
		return new ItemDAO().update(item);
	}
	
	
	public List<Item> getItemListCompraBD(int idCompra) throws SQLException {
		return new ItemDAO().listAll(idCompra);
	}

	public int excluirItem(int idItem) throws SQLException {
		return new ItemDAO().delete(idItem);	
	}

}
