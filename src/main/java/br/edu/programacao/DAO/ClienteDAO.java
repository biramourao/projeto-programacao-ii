package br.edu.programacao.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.programacao.model.Cliente;

public class ClienteDAO{

	/**
	 * Metodo para inserção de um Cliente no Banco de dados.
	 * 
	 * @param  Cliente
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * @throws SQLException
	 */
	/**
	 * <b>insert</b><br>
	 * <br>
	 * public int insert(Cliente obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para inserção de um Cliente no banco de dados.
	 * 
	 * @param obj Objeto Cliente com as informações a ser inseridas no BD
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int insert(Cliente obj) throws SQLException {
		int id = proximoId();
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("INSERT INTO cliente VALUES (?,?,?,?,?)");
		
		//geraçao do código de identificaçao
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		ps.setLong(1, Long.parseLong(sdf.format(timestamp)));*/
		ps.setInt(1, id);
		ps.setString(2, obj.getCpf());
		ps.setString(3, obj.getNome());
		ps.setString(4, obj.getEndereco());
		ps.setString(5, obj.getSexo());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		//fechando consulta
		ps.close();
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
		
	}
	
	/**
	 * <b>update</b><br>
	 * <br>
	 * public int update(int id, Cliente obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para atualizaçao de um Cliente no banco de dados.
	 * 
	 * @param id Identificaçao do cliente a ser alterado
	 * @param obj Objeto Cliente com as novas informações alteradas
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int update(Cliente obj) throws SQLException {
		
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("UPDATE cliente SET "
				+ "idcliente = ?,"
				+ "cpfcliente = ?, "
				+ "nome = ?, "
				+ "endereco = ?, "
				+ "sexo = ? "
				+ "WHERE idcliente = ?");
		
		ps.setInt(1, obj.getIdCliente());
		ps.setString(2, obj.getCpf());
		ps.setString(3, obj.getNome());
		ps.setString(4, obj.getEndereco());
		ps.setString(5, obj.getSexo());
		ps.setLong(6, obj.getIdCliente());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}

	/**
	 * <b>delete</b><br>
	 * <br>
	 * public int delete(String id) throws SQLException;
	 * <br>
	 * <br>
	 * Método para exclusao de um Cliente no banco de dados.
	 * 
	 * @param id Identificaçao do cliente a ser excluido
	 * 
	 * @return 0 ou 1 (0= mal-sucedido(cliente não encontrado) e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int delete(int id) throws SQLException {
		
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("DELETE FROM cliente WHERE idcliente = ?");
		ps.setInt(1, id);
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}
	/**
	 * <b>listAll</b><br>
	 * <br>
	 * public List<Cliente> listAll() throws SQLException;
	 * <br>
	 * <br>
	 * Método para listar todos os Clientes cadastrados no banco de dados.
	 * 
	 * @return Collection de Cliente
	 * 
	 * @throws SQLException 
	 */
	public List<Cliente> listAll() throws SQLException {
		
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM cliente");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		 
        //criando lista
		List<Cliente> clientes = new ArrayList<Cliente>();
		
		//adicionando resultados a lista
        while (rs.next()) {
            Cliente cliente = new Cliente();
            
            cliente.setIdCliente(rs.getInt("idcliente"));
            cliente.setCpf(rs.getString("cpfcliente"));
            cliente.setNome(rs.getString("nome"));
            cliente.setEndereco(rs.getString("endereco"));
            cliente.setSexo(rs.getString("sexo"));
            
            clientes.add(cliente);
        }
        //fechando consulta
  		ps.close();
  		
  		//fechando conexao com o BD
  		connection.close();
		return clientes;
	}

	/**
	 * <b>readOne</b><br>
	 * <br>
	 * public Cliente readOne(String id) throws SQLException
	 * <br>
	 * <br>
	 * Método para buscar um Cliente especifico cadastrado no banco de dados.
	 * 
	 * @param cpf CPF do Cliente a ser procurado
	 * @return Objeto Cliente com as informações trazidas do BD
	 * 
	 * @throws SQLException 
	 */
	public Cliente readOne(int id) throws SQLException {
		
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM cliente WHERE idcliente = ?");
		ps.setInt(1, id);
		
		//executando a consulta
		ps.executeQuery();
		
		//guardando o retorno
		ResultSet rs = ps.getResultSet();
		
		//resgatando o resultado
		rs.next();
		
		//criando e setando os valores no objeto
		Cliente cliente = new Cliente();
		cliente.setIdCliente(rs.getInt("idcliente"));
        cliente.setCpf(rs.getString("cpfcliente"));
        cliente.setNome(rs.getString("nome"));
        cliente.setEndereco(rs.getString("endereco"));
        cliente.setSexo(rs.getString("sexo"));
        
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return cliente;
	}
	public int proximoId() throws SQLException {
		
		// select max(campo_id) from tabela
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT MAX(idcliente) from cliente");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		int ultimoId = 0;
		//adicionando resultados a lista
        while (rs.next()) {
            ultimoId = rs.getInt("MAX(idcliente)");
        }
        
        ps.close();
        
        connection.close();
        
		return (ultimoId+1);
		
		
	}

}
