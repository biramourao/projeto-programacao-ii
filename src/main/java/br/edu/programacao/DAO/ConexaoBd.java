package br.edu.programacao.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class ConexaoBd {
	
	private static Connection connection = null;
	private static String bdLocation = "jdbc:mysql://localhost:3306/projetoprogramacaoiibd";
	private static String user = "root";
	private static String password = "root"; //Casa e Trabalho
	//private static String password = "aluno"; //Estacio

	public static Connection getConnection() throws SQLException {
			
		if (connection == null) {
			connection = DriverManager.getConnection(bdLocation,user,password);
		}else {
			connection.close();
			connection = DriverManager.getConnection(bdLocation,user,password);
		}
		return connection;
	}
}
