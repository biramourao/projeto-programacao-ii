package br.edu.programacao.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.programacao.model.Item;
import br.edu.programacao.model.Produto;

public class ItemDAO{

	/**
	 * Metodo para inserção de um Item no Banco de dados.
	 * 
	 * @param  Item
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * @throws SQLException
	 *
	 * <b>insert</b><br>
	 * <br>
	 * public int insert(Item obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para inserção de um Item no banco de dados.
	 * 
	 * @param obj Objeto Item com as informações a ser inseridas no BD
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int insert(Item obj) throws SQLException {
		int id = proximoId();
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("INSERT INTO item VALUES (?,?,?,?)");
		ps.setInt(1, id);
		ps.setDouble(2, obj.getQuantidade());
		ps.setInt(3, obj.getIdCompra());
		ps.setLong(4, obj.getProduto().getIdProduto());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		//fechando consulta
		ps.close();
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}

	public int update(Item obj) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int delete(int id) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("DELETE FROM item WHERE iditem = ?");
		ps.setInt(1, id);
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}

	public List<Item> listAll(int idCompra) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT item.iditem, item.qtd, item.compra_idcompra, produto.idproduto, produto.descricao, produto.preco FROM item " + 
				"INNER JOIN produto " +
				"ON produto.idproduto = item.produto_idproduto WHERE compra_idcompra = ?");
		ps.setInt(1, idCompra);
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		 
        //criando lista
		List<Item> itens = new ArrayList<Item>();
		
		//adicionando resultados a lista
        while (rs.next()) {
        	Item item = new Item();
        	Produto produto = new Produto();
        	
        	produto.setIdProduto(rs.getInt("idproduto"));
        	produto.setDescricao(rs.getString("descricao"));
        	produto.setPreco(rs.getDouble("preco"));
        	
            item.setIdCompra(rs.getInt("compra_idcompra"));
            item.setIdItem(rs.getInt("iditem"));
            item.setProduto(produto);
            item.setQuantidade(rs.getInt("qtd"));
            itens.add(item);
        }
        
        ps.close();
        connection.close();
        
		return itens;
	}

	public Item readOne(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	public int proximoId() throws SQLException {
		
		// select max(campo_id) from tabela
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT MAX(iditem) from item");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		int ultimoId = 0;
		//adicionando resultados a lista
        while (rs.next()) {
            ultimoId = rs.getInt("MAX(iditem)");
        }
        
        ps.close();
        
        connection.close();
        
		return (ultimoId+1);
		
		
	}
	/*
	 * SELECT cliente.nome, compra.idcompra, item.iditem, item.qtd, produto.descricao, produto.preco
		FROM item
		INNER JOIN produto 
		INNER JOIN cliente
		INNER JOIN compra
		ON produto.idproduto = item.produto_idproduto
		and item.compra_idcompra = compra.idcompra
		and cliente.idcliente = compra.cliente_idcliente;
	 * */

}
