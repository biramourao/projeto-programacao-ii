package br.edu.programacao.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.programacao.model.Produto;

public class ProdutoDAO implements ICrudDAO<Produto>{

	/**
	 * Metodo para inserção de um Produto no Banco de dados.
	 * 
	 * @param  Produto
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * @throws SQLException
	 *
	 * <b>insert</b><br>
	 * <br>
	 * public int insert(Produto obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para inserção de um Produto no banco de dados.
	 * 
	 * @param obj Objeto Produto com as informações a ser inseridas no BD
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int insert(Produto obj) throws SQLException {
		int id = proximoId();
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("INSERT INTO produto VALUES (?,?,?)");
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
		ps.setLong(1, Long.parseLong(sdf.format(timestamp)));*/
		ps.setInt(1, id);
		ps.setString(2, obj.getDescricao());
		ps.setDouble(3, obj.getPreco());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		//fechando consulta
		ps.close();
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}
	
	/**
	 * <b>update</b><br>
	 * <br>
	 * public int update(int id, Produto obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para atualizaçao de um Produto no banco de dados.
	 * 
	 * @param id Identificaçao do produto a ser alterado
	 * @param obj Objeto Produto com as novas informações alteradas
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int update(Produto obj) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("UPDATE produto SET "
				+ "idproduto = ?, "
				+ "descricao = ?, "
				+ "preco = ? "
				+ "WHERE idproduto = ?");
		
		ps.setInt(1, obj.getIdProduto());
		ps.setString(2, obj.getDescricao());
		ps.setDouble(3, obj.getPreco());
		ps.setInt(4, obj.getIdProduto());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		//fechando consulta
		ps.close();
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}
	/**
	 * <b>delete</b><br>
	 * <br>
	 * public int delete(int id) throws SQLException;
	 * <br>
	 * <br>
	 * Método para exclusao de um produto no banco de dados.
	 * 
	 * @param id Identificaçao do produto a ser excluído
	 * 
	 * @return 0 ou 1 (0= mal-sucedido(cliente não encontrado) e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int delete(int id) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("DELETE FROM produto WHERE idproduto = ?");
		ps.setInt(1, id);
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}
	/**
	 * <b>listAll</b><br>
	 * <br>
	 * public Collection<Produto> listAll() throws SQLException;
	 * <br>
	 * <br>
	 * Método para listar todos os produtos cadastrados no banco de dados.
	 * 
	 * @return Collection de Produto
	 * 
	 * @throws SQLException 
	 */
	public List<Produto> listAll() throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM produto");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		 
        //criando lista
		List<Produto> produtos = new ArrayList<Produto>();
		
		//adicionando resultados a lista
        while (rs.next()) {
            Produto produto = new Produto();
            
            produto.setIdProduto(rs.getInt("idproduto"));
            produto.setDescricao(rs.getString("descricao"));
            produto.setPreco(rs.getDouble("preco"));
            
            produtos.add(produto);
        }
        ps.close();
        connection.close();
		return produtos;
	}
	/**
	 * <b>readOne</b><br>
	 * <br>
	 * public Produto readOne(String id) throws SQLException
	 * <br>
	 * <br>
	 * Método para buscar um produto especifico cadastrado no banco de dados.
	 * 
	 * @param id id do produto a ser procurado
	 * @return Objeto Produto com as informações trazidas do BD
	 * 
	 * @throws SQLException 
	 */
	public Produto readOne(int id) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM produto WHERE idproduto = ?");
		ps.setInt(1, id);
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		 
        //criando lista
		Produto produto = new Produto();
		
		//adicionando resultados a lista
        while (rs.next()) {

            produto.setIdProduto(rs.getInt("idproduto"));
            produto.setDescricao(rs.getString("descricao"));
            produto.setPreco(rs.getDouble("preco"));
        }
        
        ps.close();
        connection.close();
        
		return produto;
	}

	public int proximoId() throws SQLException {
		
		// select max(campo_id) from tabela
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT MAX(idproduto) from produto");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		int ultimoId = 0;
		//adicionando resultados a lista
        while (rs.next()) {
            ultimoId = rs.getInt("MAX(idproduto)");
        }
        
        ps.close();
        
        connection.close();
        
		return (ultimoId+1);
		
		
	}
	

}
