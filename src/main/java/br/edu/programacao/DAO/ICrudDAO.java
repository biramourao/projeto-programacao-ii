package br.edu.programacao.DAO;

import java.sql.SQLException;
import java.util.Collection;

public interface ICrudDAO<T> {

	public int insert(T obj) throws SQLException;
	public int update(T obj) throws SQLException;
	public int delete(int id) throws SQLException;
	public Collection<T> listAll() throws SQLException;
	public T readOne(int id) throws SQLException;
	public int proximoId() throws SQLException;
	
}
