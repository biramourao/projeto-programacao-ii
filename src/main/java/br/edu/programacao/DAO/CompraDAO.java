package br.edu.programacao.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.programacao.model.Cliente;
import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;
import br.edu.programacao.model.Produto;

public class CompraDAO implements ICrudDAO<Compra>{

	/**
	 * Metodo para inserção de uma Compra no Banco de dados.
	 * 
	 * @param  Compra
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * @throws SQLException
	 *
	 * <b>insert</b><br>
	 * <br>
	 * public int insert(Compra obj) throws SQLException;
	 * <br>
	 * <br>
	 * Método para inserção de um Compra no banco de dados.
	 * 
	 * @param obj Objeto Compra com as informações a ser inseridas no BD
	 * 
	 * @return 0 ou 1 (0= mal-sucedido e 1=bem-sucedido)
	 * 
	 * @throws SQLException 
	 */
	public int insert(Compra obj) throws SQLException {
		int id = proximoId();
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("INSERT INTO compra VALUES (?,?)");
		ps.setInt(1, id);
		ps.setLong(2, obj.getCliente().getIdCliente());
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		//fechando consulta
		ps.close();
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}

	public int update(Compra obj) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	public int delete(int id) throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("DELETE FROM compra WHERE idcompra = ?");
		ps.setInt(1, id);
		
		//executando a consulta e guardando o retorno
		int resultado = ps.executeUpdate();
		
		//fechando consulta
		ps.close();
		
		//fechando conexao com o BD
		connection.close();
		
		return resultado;
	}

	public List<Compra> listAll() throws SQLException {
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		/*PreparedStatement ps = connection.prepareStatement
				("SELECT cliente.*, compra.idcompra, item.*, produto.preco " + 
				"FROM item " + 
				"INNER JOIN produto " + 
				"INNER JOIN cliente " + 
				"INNER JOIN compra " + 
				"ON produto.idproduto = item.produto_idproduto " + 
				"and item.compra_idcompra = compra.idcompra " + 
				"and cliente.idcliente = compra.cliente_idcliente");*/
		PreparedStatement ps = connection.prepareStatement("SELECT * FROM compra "
				+ "INNER JOIN cliente "
				+ "ON cliente.idcliente = compra.cliente_idcliente");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		 
        //criando lista
		List<Compra> compras = new ArrayList<Compra>();
		
		//adicionando resultados a lista
        while (rs.next()) {
        	
        	Compra compra = new Compra();
        	Cliente cliente = new Cliente();
        	cliente.setIdCliente(rs.getInt("idcliente"));
        	cliente.setCpf(rs.getString("cpfcliente"));
        	cliente.setNome(rs.getString("nome"));
        	cliente.setSexo(rs.getString("sexo"));
        	cliente.setEndereco(rs.getString("endereco"));
        	
        	compra.setCliente(cliente);
        	compra.setIdCompra(rs.getInt("idcompra"));
        	
        	compras.add(compra);
        }
        
        PreparedStatement ps2 = connection.prepareStatement("SELECT item.*, produto.* "
        		+ "FROM item "
        		+ "INNER JOIN produto "
        		+ "ON produto.idproduto = item.produto_idproduto "
        		+ "WHERE compra_idcompra = ?");
		
		
		
		//adicionando resultados a lista
		for(Compra c : compras) {
			ps2.setInt(1, c.getIdCompra());
			//Executando a consulta
			ps2.executeQuery();
			
			//tratando o resultado
			ResultSet rs2 = ps2.getResultSet();
			
			while (rs2.next()) {
				
				Item item = new Item();
	        	Produto produto = new Produto();
	        	
	        	produto.setIdProduto(rs2.getInt("idproduto"));
	        	produto.setDescricao(rs2.getString("descricao"));
	        	produto.setPreco(rs2.getDouble("preco"));
	        	
	            item.setIdCompra(rs2.getInt("compra_idcompra"));
	            item.setIdItem(rs2.getInt("iditem"));
	            item.setProduto(produto);
	            item.setQuantidade(rs2.getInt("qtd"));
	            
	            c.adicionarItem(item);
	        }
		}
		
        ps.close();
        connection.close();
        
		return compras;
	}

	public Compra readOne(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	public int proximoId() throws SQLException {
		
		// select max(campo_id) from tabela
		//Abrindo Conexão com o BD
		Connection connection = ConexaoBd.getConnection();
		
		//Preparando a consulta
		PreparedStatement ps = connection.prepareStatement("SELECT MAX(idcompra) from compra");
		
		//Executando a consulta
		ps.executeQuery();
		
		//tratando o resultado
		ResultSet rs = ps.getResultSet();
		int ultimoId = 0;
		//adicionando resultados a lista
        while (rs.next()) {
            ultimoId = rs.getInt("MAX(idcompra)");
        }
        
        ps.close();
        
        connection.close();
        
		return (ultimoId+1);
		
		
	}

}
