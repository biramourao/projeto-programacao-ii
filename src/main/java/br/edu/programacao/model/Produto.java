package br.edu.programacao.model;

public class Produto {
	private int idProduto;
	private String descricao;
	private double preco;
	
	 public Produto(int pIdProduto, String pDescricao, double pPreco) {
		idProduto = pIdProduto;
		descricao = pDescricao;
		preco = pPreco;
	}
	
	public Produto() {
		
	}

	public int getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	 
	@Override
	public String toString() {
		
		return "\n--- Produto ---"
				+ "\nId: " + getIdProduto()
				+ "\nDescricao: " + getDescricao()
				+ "\nPreço: " + getPreco();
		
	}
}
