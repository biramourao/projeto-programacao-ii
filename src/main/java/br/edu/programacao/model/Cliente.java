package br.edu.programacao.model;

public class Cliente {
	
	private int idCliente;
	private String cpf;
	private String nome;
	private String sexo;
	private String endereco;
	
	public Cliente() {
		
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	
	@Override
	public String toString() {
		return "\n-- Cliente --"
				+ "\nId: " + getIdCliente()
				+ "\nCPF: " + getCpf()
				+ "\nNome: " + getNome()
				+ "\nSexo " + getSexo()
				+ "\nEndereço: " + getEndereco()
				+ "\n";
	}

	
}
