package br.edu.programacao.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class Compra {
	private int idCompra;
	private Collection<Item> itens = new HashSet<Item>();
	private Cliente cliente = new Cliente();
	
	public Compra() {
	
	}
	
	public int getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}

	public Collection<Item> getItens() {
		return itens;
	}

	/**
	 * Método para adicionar um novo item a compra atual.
	 * @param item
	 */
	public void adicionarItem(Item item) {
		this.itens.add(item);
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public String imprimeItens () {
		Iterator<Item> it = itens.iterator();
		String stringItens = "\n";
		while(it.hasNext()){
            Item item = (Item)it.next();
            stringItens += item.toString();
        }
		return stringItens;
	}

	@Override
	public String toString() {

		return "\n-- Compra --"
				+ "\nId: " + getIdCompra()
				+ "\nItens: " + imprimeItens()
				+ getCliente();
				
	}
}
