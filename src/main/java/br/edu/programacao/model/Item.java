package br.edu.programacao.model;

public class Item {
	
	private int idItem;
	private double quantidade;
	private int idCompra;
	private Produto produto;
	
	public Item() {
		
	}

	public int getIdItem() {
		return idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public int getIdCompra() {
		return idCompra;
	}

	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}

	@Override
	public String toString() {
		return "\n--Item--"
				+ "\n Id Compra: " + getIdCompra()
				+ "\nId Item: " +  getIdItem()
				+ "\nQuantidade: " + getQuantidade()
				+ getProduto();
	}
}
