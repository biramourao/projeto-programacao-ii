package br.edu.programacao.testes;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import br.edu.programacao.DAO.ClienteDAO;
import br.edu.programacao.DAO.ItemDAO;
import br.edu.programacao.DAO.ProdutoDAO;
import br.edu.programacao.model.Cliente;
import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;
import br.edu.programacao.model.Produto;

public class TesteItemDAO {

	public static void main(String[] args) {
		
		Cliente cliente = new Cliente();
		cliente.setIdCliente(3);
		cliente.setCpf("00000000003");
		cliente.setNome("Ubiratan L Mourao");
		cliente.setSexo("m");
		cliente.setEndereco("Qd: 112, Lt: 29 - Centro - SAD-GO");
		
		ClienteDAO clienteDAO = new ClienteDAO();
		try {
			int resultado = clienteDAO.insert(cliente);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Cliente Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		Produto produto = new Produto();
		produto.setIdProduto(1);
		produto.setDescricao("Xbox One");
		produto.setPreco(1200.00);
		System.out.println(produto);
		
		Compra compra = new Compra();
		compra.setCliente(cliente);
		compra.setIdCompra(1);
		
		//teste de insert
		ProdutoDAO produtoDAO = new ProdutoDAO();
		try {
			int resultado = produtoDAO.insert(produto);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, produto.getDescricao()+" Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir produto. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		Item item = new Item();
		item.setIdItem(1);
		item.setQuantidade(2);
		item.setIdCompra(1);
		item.setProduto(produto);
		
		ItemDAO itemDAO = new ItemDAO();
		try {
			int resultado = itemDAO.insert(item);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Item Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir item. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}

	}

}
