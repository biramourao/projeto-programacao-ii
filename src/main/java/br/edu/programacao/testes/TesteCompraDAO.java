package br.edu.programacao.testes;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.edu.programacao.DAO.CompraDAO;
import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;

public class TesteCompraDAO {

	public static void main(String[] args) {

		/*
		 * Cliente cliente = new Cliente(); cliente.setIdCliente(3);
		 * cliente.setCpf("00000000003"); cliente.setNome("Ubiratan L Mourao");
		 * cliente.setSexo("m");
		 * cliente.setEndereco("Qd: 112, Lt: 29 - Centro - SAD-GO");
		 * 
		 * ClienteDAO clienteDAO = new ClienteDAO(); try { int resultado =
		 * clienteDAO.insert(cliente); if (resultado == 1) {
		 * JOptionPane.showMessageDialog(null, "Cliente Inserido com Sucesso","Sucesso",
		 * JOptionPane.INFORMATION_MESSAGE); } } catch (SQLException e) {
		 * JOptionPane.showMessageDialog(null, "Erro ao inserir cliente. \nDetalhes: " +
		 * e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE); }
		 * 
		 * Compra compra = new Compra(); compra.setCliente(cliente);
		 * compra.setIdCompra(2);
		 * 
		 * CompraDAO compraDAO = new CompraDAO(); try { int resultado =
		 * compraDAO.insert(compra); if (resultado == 1) {
		 * JOptionPane.showMessageDialog(null, "Compra Inserida com Sucesso","Sucesso",
		 * JOptionPane.INFORMATION_MESSAGE); } } catch (SQLException e) {
		 * JOptionPane.showMessageDialog(null, "Erro ao inserir compra. \nDetalhes: " +
		 * e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE); }
		 */
		List<Compra> compras = new ArrayList<Compra>();
		int cont = 0;
		try {
			compras = new CompraDAO().listAll();
			for (Compra compra : compras) {
				System.out.println("Compra " + cont);
				System.out.println();
				System.out.println("cpf: " + compra.getCliente().getCpf());
				System.out.println("nome: " + compra.getCliente().getNome());
				System.out.println("id Compra: " + compra.getIdCompra());
				System.out.println("Qtd Itens: " + compra.getItens().size());
				double total = 0;
				for (Item item : compra.getItens()) {
					total += item.getQuantidade() * item.getProduto().getPreco();
				}
				System.out.println("Valor total: " + total);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
