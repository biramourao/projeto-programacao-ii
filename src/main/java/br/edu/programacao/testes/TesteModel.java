package br.edu.programacao.testes;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import br.edu.programacao.model.Cliente;
import br.edu.programacao.model.Compra;
import br.edu.programacao.model.Item;
import br.edu.programacao.model.Produto;

public class TesteModel {

	public static void main(String[] args) {
				
		Cliente cliente = new Cliente();
		cliente.setCpf("000.000.000-01");
		cliente.setNome("Ubiratan L Mourao");
		cliente.setSexo("m");
		cliente.setEndereco("Qd: 112, Lt: 29 - Centro - SAD-GO");
		System.out.println(cliente);
		
		Produto produto = new Produto();
		produto.setIdProduto(1);
		produto.setDescricao("Xbox One");
		produto.setPreco(2500.00);
		System.out.println(produto);
		
		Compra compra = new Compra();
		compra.setCliente(cliente);
		compra.setIdCompra(1);
		
		Item item = new Item();
		item.setIdCompra(1);
		item.setIdItem(1);
		item.setProduto(produto);
		item.setQuantidade(1);
		
		Item item2 = new Item();
		item2.setIdCompra(1);
		item2.setIdItem(2);
		item2.setProduto(produto);
		item2.setQuantidade(3);
		
		compra.adicionarItem(item);
		compra.adicionarItem(item2);
		System.out.println(compra);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		System.out.println("Milisegundos: " + Long.parseLong(sdf.format(timestamp)));
	}

}
