package br.edu.programacao.testes;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JOptionPane;

import br.edu.programacao.DAO.ProdutoDAO;
import br.edu.programacao.model.Produto;

public class TesteProdutoDAO {

	public static void main(String[] args) throws SQLException {
		
		Produto produto = new Produto();
		produto.setIdProduto(1);
		produto.setDescricao("Xbox One");
		produto.setPreco(1200.00);
		System.out.println(produto);
		
		//teste de insert
		ProdutoDAO produtoDAO = new ProdutoDAO();
		try {
			int resultado = produtoDAO.insert(produto);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, produto.getDescricao()+" Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir produto. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		
		 //teste de update
		int idProduto = produto.getIdProduto();
		produto.setDescricao("Xbox One X");
		produto.setPreco(2500.60);
		
		try {
			int resultado = produtoDAO.update(produto);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, produto.getDescricao()+" alterado com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste de delete
		idProduto = 1;
		try {
			int resultado = produtoDAO.delete(idProduto);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, produto.getDescricao()+" excluido com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, "Erro ao excluir cliente. \nDetalhes: Cliente não encontrado!", "Erro!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste de listAll
		try {
			Collection<Produto> resultado = produtoDAO.listAll();
			if (resultado.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Nenhum cliente encontrado!","Informação", JOptionPane.INFORMATION_MESSAGE);
			}else {
				String dadosProdutos = "| id | \tdescricao | \tpreco |\n";
				Iterator<Produto> produtos = resultado.iterator();
	               while (produtos.hasNext()){
	                      Produto it = produtos.next();
	                      dadosProdutos += "| "+it.getIdProduto() + "| \t" + it.getDescricao() + "| \t" + it.getPreco() + "| \n";
	               }
				JOptionPane.showMessageDialog(null, dadosProdutos, "Lista de Produos!", JOptionPane.PLAIN_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao listar produtos. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste readOne
		idProduto = 3;
		try {
			Produto produto2 = produtoDAO.readOne(idProduto);
			if (produto2 != null) {
				String dadosProdutos = "| id | \tdescricao | \tpreco |\n"+
						"| "+produto2.getIdProduto() + "| \t" + produto2.getDescricao() + "| \t" + produto2.getPreco() + "| \n";
				JOptionPane.showMessageDialog(null, dadosProdutos, "Sucesso", JOptionPane.PLAIN_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, "Erro ao consultar cliente. \nDetalhes: Cliente não encontrado!", "Erro!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao consultar cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		

	}

}
