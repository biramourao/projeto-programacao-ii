package br.edu.programacao.testes;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.JOptionPane;

import br.edu.programacao.DAO.ClienteDAO;
import br.edu.programacao.model.Cliente;

public class TesteClienteDAO {

	public static void main(String[] args) throws SQLException {
		
		Cliente cliente = new Cliente();
		cliente.setIdCliente(3);
		cliente.setCpf("00000000003");
		cliente.setNome("Ubiratan L Mourao");
		cliente.setSexo("m");
		cliente.setEndereco("Qd: 112, Lt: 29 - Centro - SAD-GO");
		System.out.println(cliente);
		
		//teste de insert
		ClienteDAO clienteDAO = new ClienteDAO();
		try {
			int resultado = clienteDAO.insert(cliente);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Cliente Inserido com Sucesso","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao inserir cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		
		 //teste de update
		int codigoCliente = cliente.getIdCliente();
		cliente.setCpf("00000000045");
		
		try {
			int resultado = clienteDAO.update(cliente);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Cliente alterado com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste de delete
		codigoCliente = 3;
		try {
			int resultado = clienteDAO.delete(codigoCliente);
			if (resultado == 1) {
				JOptionPane.showMessageDialog(null, "Cliente excluido com Sucesso!","Sucesso", JOptionPane.INFORMATION_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, "Erro ao excluir cliente. \nDetalhes: Cliente não encontrado!", "Erro!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste de listAll
		try {
			Collection<Cliente> resultado = clienteDAO.listAll();
			if (resultado.isEmpty()) {
				JOptionPane.showMessageDialog(null, "Nenhum cliente encontrado!","Informação", JOptionPane.INFORMATION_MESSAGE);
			}else {
				String dadosClientes = "id \tCPF \tNome \tSexo \tEndereco\n";
				Iterator<Cliente> clientes = resultado.iterator();
	               while (clientes.hasNext()){
	                      Cliente it = clientes.next();
	                      dadosClientes += it.getIdCliente() + "\t" + it.getCpf() + "\t" + it.getNome() + "\t" + it.getSexo() + "\t" + it.getEndereco() + "\n";
	               }
				JOptionPane.showMessageDialog(null, dadosClientes, "Lista de Clientes!", JOptionPane.PLAIN_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao listar clientes. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}
		
		//teste readOne
		/*codigoCliente = 2;
		try {
			Cliente cliente2 = clienteDAO.readOne(codigoCliente);
			if (cliente2 != null) {
				String dadosClientes = "|CPF \t| Nome \t| Sexo \t| Endereco|\n|"+
				cliente2.getCpf() + "\t| " + cliente2.getNome() + "\t| " + cliente2.getSexo() + "\t| " + cliente2.getEndereco() + "\n|";
				JOptionPane.showMessageDialog(null, dadosClientes, "Sucesso", JOptionPane.PLAIN_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, "Erro ao consultar cliente. \nDetalhes: Cliente não encontrado!", "Erro!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao consultar cliente. \nDetalhes: " + e.getMessage(), "Erro!", JOptionPane.ERROR_MESSAGE);
		}*/
		
		

	}

}
